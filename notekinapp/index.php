<?php 

    session_start();
        
    if(!isset($_SESSION["loggedin"]) || $_SESSION["loggedin"] !== true) {
        header("location: login.php");
        exit;
    } else {
        $currentUser = $_SESSION["userLogged"];
    }

    require_once "loadNotesFromUser.php";

    $noteList = "";
    for ($i = 0; $i < count($notes); $i++) { 
        $noteList = $noteList . "<li id=" . $notes[$i]["ID"] . ">" . $notes[$i]["Note_Title"] . "</li>";
    }
    $notesjson = json_encode($notes);
    $reloadPage = "<meta http-equiv='refresh' content='0'>";

    if ($_SERVER["REQUEST_METHOD"] == "POST") {
        $currentNoteID = $_POST["currentNoteID"];
        $currentNoteTitle = $_POST["currentNoteTitle"];
        $currentNoteText = $_POST["currentNote"];

        if ($_POST['save'] == 'Save') {
            $saveNoteStmt = "UPDATE notes SET Note_Title = '$currentNoteTitle', Note_Text= '$currentNoteText' WHERE ID = '$currentNoteID'";
            $saveNoteQuery = mysqli_query( $link, $saveNoteStmt );
            echo $reloadPage;
        }

        if ($_POST['save'] == 'Save As New Note') {
            $saveNewNoteStmt = "INSERT INTO notes (Username, Note_Title, Note_Text) VALUES ('$currentUser','$currentNoteTitle', '$currentNoteText')";
            $saveNewNoteQuery = mysqli_query( $link, $saveNewNoteStmt );
            echo $reloadPage;
        }
    }

?>
<!DOCTYPE html>
<html lang="en">
    <head>
        <meta charset="utf-8">
        <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
        <script
			  src="https://code.jquery.com/jquery-3.4.1.js"
			  integrity="sha256-WpOohJOqMqqyKL9FccASB9O0KwACQJpFTUBLTYOVvVU="
			  crossorigin="anonymous"></script>
        <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.12.9/umd/popper.min.js" integrity="sha384-ApNbgh9B+Y1QKtv3Rn7W3mgPxhU9K/ScQsAP7hUibX39j7fakFPskvXusvfa0b4Q" crossorigin="anonymous"></script>
        <link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.6.3/css/all.css" integrity="sha384-UHRtZLI+pbxtHCWp1t77Bi1L4ZtiqrqD80Kn4Z8NTSRyMA2Fd33n5dQ8lWUE00s/" crossorigin="anonymous">
        <link rel="stylesheet" href="https://bootswatch.com/4/darkly/bootstrap.min.css" />
        <script>
            // Creates a JS global variable notes with json array from php code
            var notes = <?php echo($notesjson)?>;
        </script>
        <script src="scripts.js"></script>
        <title>NOTEKIN | Home</title>
    </head>
    <body>
        <div class="container-fluid mb-4 pt-0 pb-0">
        <nav class="navbar navbar-expand-md navbar-dark bg-dark pt-0 pb-0">
                
                <img class="mr-3" style="height: 110px" src="img/notekin_logo.png" alt="Notekin Logo">
                <button type="button" class="navbar-toggler" data-toggle="collapse" data-target="#navbarCollapse">
                    <span class="navbar-toggler-icon"></span>
                </button>
            
                <div class="collapse navbar-collapse justify-content-between" id="navbarCollapse">
                    <div class="navbar-nav">
                        <a href="index.php" class="nav-item nav-link active">Home</a>
                        <a href="about.html" class="nav-item nav-link">About</a>
                        
                    </div>
                    <div><p><?php echo "Hello, <strong> $currentUser  </strong>! Welcome to your favorite notes web app!" ?></p></div>
                    
                    <div class="navbar-nav">
                        <a href="logout.php" class="nav-item nav-link">Logout</a>
                    </div>
                </div>
                
            </nav>
        </div>

        <div class="container-fluid">
            <div class="jumbotron align-items-center">
                <div class="col-12 mb-2">
                    <h4> Your notes list: </h4>
                    <ul id="notesList">
                       <?php echo $noteList ; ?>

                    </ul>
                    <p>please click on the note to open on editor..</p>
                </div>

            </div>
        </div>

        <div class="container-fluid">
            <div class="jumbotron align-items-center">
                <div class="col-12 mb-2">
                    <form method="POST" action="index.php">
                    <input type="hidden" id="currentNoteID" name="currentNoteID" value="">
                    <button type="button" id="discard" name="discard" class="btn btn-danger">Discard </button>
                    <input type="submit" id="save" name="save" class="btn btn-primary" value="Save">
                    <input type="submit" id="saveNew" name="save" class="btn btn-info" value="Save As New Note">
                </div>

                <div class="col-12 form-group shadow-textarea">
                    <textarea class="form-control z-depth-1" name="currentNoteTitle" id="currentNoteTitle" cols="10" rows="1" value=""></textarea>
                </div>

                <div class="col-12 form-group shadow-textarea">
                    <textarea class="form-control z-depth-1"  name="currentNote" id="currentNote" cols="160" rows="300" value="" autofocus></textarea>
                </div>
                </form>
            </div>
        </div>
        
        
        
    </body>
    
</html>