<?php
    session_start();
    
    if(isset($_SESSION["loggedin"]) && $_SESSION["loggedin"] === true){
      header("location: index.php");
      exit;
    }
    
    // Setting up MySQL connection
    $host = 'mysql';
    $dbUser = 'root';
    $dbPass = 'root';
    $dbName = 'notekin';
    $link = mysqli_connect( $host, $dbUser, $dbPass, $dbName );
    if (!$link) {
      echo "Error: Unable to connect to MySQL." . PHP_EOL;
      echo "Debugging errno: " . mysqli_connect_errno() . PHP_EOL;
      echo "Debugging error: " . mysqli_connect_error() . PHP_EOL;
      exit;
    }
    
    // Initializing variables
    $user = "";
    $user_error = "";
    $pass = "";
    $pass_error = "";
    
    // Checking form data
    if ($_SERVER["REQUEST_METHOD"] == "POST") {
      
      // Check if username is empty
      if(empty(trim($_POST["user"]))) {
        $user_error = "We need your username !";
      } else {
        $user = trim($_POST["user"]);
      }
      
      // Checking if password is empty
      if(empty(trim($_POST["pass"]))) {
        $pass_error = "We need your password !";
      } else {
        $pass = trim($_POST["pass"]);
      }
      
      // Checking if user and pass matches
      if (!empty($user) && !empty($pass)) {
        $getUsernameStmt = "SELECT Username, Pass FROM users WHERE Username= ? ";
        
        if($statementPrepared = mysqli_prepare($link, $getUsernameStmt)) {
          mysqli_stmt_bind_param($statementPrepared, "s", $userParameter);
          
          $userParameter = $user;
          
          if(mysqli_execute($statementPrepared)) {
            mysqli_stmt_store_result($statementPrepared);
            
            if(mysqli_stmt_num_rows($statementPrepared) == 1) {
              mysqli_stmt_bind_result($statementPrepared, $userFromDB, $passFromDB);
              
              if(mysqli_stmt_fetch($statementPrepared)) {
                if($pass == $passFromDB) {
                  session_start();
                  
                  $_SESSION["loggedin"] = true;
                  $_SESSION["userLogged"] = $userFromDB;
                  
                  header("location: index.php");
                }
                else {
                  $pass_error = "Wrong password!";
                }
              }
            } else {
              $user_error = "Seems like you don't have a user account!";
            }
          } else {
            echo "Try again!";
          }
        }
        mysqli_stmt_close($statementPrepared);
      }
      mysqli_close($link);
    }
  
?>

<!DOCTYPE html>
<html lang="en">

<head>
  <meta charset="utf-8">
  <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
  <script
			  src="https://code.jquery.com/jquery-3.4.1.js"
			  integrity="sha256-WpOohJOqMqqyKL9FccASB9O0KwACQJpFTUBLTYOVvVU="
			  crossorigin="anonymous"></script>
		
  <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.12.9/umd/popper.min.js"
    integrity="sha384-ApNbgh9B+Y1QKtv3Rn7W3mgPxhU9K/ScQsAP7hUibX39j7fakFPskvXusvfa0b4Q"
    crossorigin="anonymous"></script>
  <link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.6.3/css/all.css"
    integrity="sha384-UHRtZLI+pbxtHCWp1t77Bi1L4ZtiqrqD80Kn4Z8NTSRyMA2Fd33n5dQ8lWUE00s/" crossorigin="anonymous">
  <link rel="stylesheet" href="https://bootswatch.com/4/darkly/bootstrap.min.css" />
  <title>NOTEKIN | Log In</title>
</head>

<body>

  <div class="container-fluid pt-0 pb-0">
    <div class="jumbotron">
      <div class="row mt-2 pt-0 pb-0">
        <div class="col-md-6 m-auto">
          <div class="card card-body">
            <h1 class="text-center mb-3"><img class="mr-3" style="height: 150px" src="img/notekin_logo.png"
                alt="Notekin Logo"> Login</h1>
            <form class="needs-validation" novalidate method="POST"
              action="<?php echo htmlspecialchars($_SERVER["PHP_SELF"]); ?>">
              <div class="form-group <?php echo (!empty($user_error)) ? 'has-error' : ''; ?>">
                <label for="user">Username</label>
                <input type="text" name="user" class="form-control" value="<?php echo $user; ?>">
                <span class="help-block"><?php echo $user_error; ?></span>
              </div>
              <div class="form-group <?php echo (!empty($pass_error)) ? 'has-error' : ''; ?>">
                <label for="pass">Password</label>
                <input type="password" name="pass" class="form-control">
                <span class="help-block"><?php echo $pass_error; ?></span>
              </div>
              <div class="form-group">
                <input type="submit" class="btn btn-primary" value="Login">
              </div>
            </form>
            
          </div>
        </div>
      </div>
    </div>
  </div>




</body>

</html>