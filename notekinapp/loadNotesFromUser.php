<?php 

    // Setting up MySQL connection
    $host = 'mysql';
    $dbUser = 'root';
    $dbPass = 'root';
    $dbName = 'notekin';
    $link = mysqli_connect( $host, $dbUser, $dbPass, $dbName );
    if (!$link) {
      echo "Error: Unable to connect to MySQL." . PHP_EOL;
      echo "Debugging errno: " . mysqli_connect_errno() . PHP_EOL;
      echo "Debugging error: " . mysqli_connect_error() . PHP_EOL;
      exit;
    }

    
    // Querying all notes from current user on database
    $getAllNotesFromUserStmt = "SELECT * FROM notes WHERE Username = '$currentUser' ";

    $notesQuery = mysqli_query( $link, $getAllNotesFromUserStmt );
    $notes = mysqli_fetch_all($notesQuery, MYSQLI_ASSOC);
  	mysqli_free_result( $notesQuery );

?>