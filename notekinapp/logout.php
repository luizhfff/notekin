<?php
    // Start session
    session_start();
    
    // Unset session variables declaring as empty array
    $_SESSION = array();
    
    // Destroy session
    session_destroy();
    
    // Redirect back to the login page
    header("location: login.php");
    exit;
?>