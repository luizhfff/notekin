$(document).ready(() => {
    var currentNoteId = "";
    var currentNote = "";
    var currentNoteTitle = "";
    
    $("#notesList li").click((event) => {
        currentNoteId = $(event.target).closest("li").attr("id");
        currentNote = notes[currentNoteId-1]['Note_Text'];
        currentNoteTitle = notes[currentNoteId-1]['Note_Title'];
        
        $("#currentNoteID").val(currentNoteId);
        $("#currentNoteTitle").val('');
        $("#currentNoteTitle").val(currentNoteTitle);
        $("#currentNote").val('');
        $("#currentNote").val(currentNote);

    });

    $("#discard").click( () => {
        clearTitle();
        clearTextArea();
    });

    function clearTitle() {
        $("#currentNoteTitle").val('');
    }

    function clearTextArea() {
        $("#currentNote").val('');
    }
});